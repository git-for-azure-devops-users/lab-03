# Git for TFS users
Lab 03: Working with Pull Requests (--no-ff merge)

---

# Tasks

 - Creating a feature branch from work item
 
 - Working with feature branch from visual studio
 
 - Working with pull requests (--no-ff merge)

---

## Creating a feature branch from work item

 - Browse to the backlog and look for your task under the PBI called "Lab-03":

&nbsp;
<img alt="Image 1.1" src="Images/1.1.PNG"  width="700" border="1">
&nbsp;


 - Click "create new branch" to create a new feature branch linked to task:

&nbsp;
<img alt="Image 1.2" src="https://docs.microsoft.com/en-us/azure/devops/boards/backlogs/_img/git-dev-pr-create-branch.png?view=azure-devops" border="1">
&nbsp;

 - Call your branch "feature/your-username" based on "master" branch:

---

## Working with feature branches from visual studio

 - Clone the repository "demo-app-lab-03" from visual studio:
    - In Team Explorer, open the Connect view
    - Select Clone under Local Git Repositories and enter the URL for your Git repo
    - Select a folder where you want your cloned repo to be kept
    - Select Clone to clone the repository

&nbsp;
<img alt="Image 1.3" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/clone_other_providers.png?view=azure-devops" border="1">
&nbsp;

 - Checkout your feature branch:
   - In the Team Explorer, open the branches view
   - Look for your looking branch, righ click and click "checkout"

&nbsp;
<img alt="Image 1.4" src="https://azuredevopslabs.com/labs/vsts/git/images/33.png" border="1">
&nbsp;

 - Perform some changes (add, commit. push/sync) - at least 3 commits

---

## Working with pull requests (--no-ff merge)

 - Open Pull Requests in Team Explorer

&nbsp;
<img alt="Image 1.5" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/pull-requests.png?view=azure-devops" border="1">
&nbsp;

 - Create a new Pull Request (from your feature into master)

&nbsp;
<img alt="Image 1.5" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/new-pull-request.png?view=azure-devops" border="1">
&nbsp;

 - Review your changes, and complete your pull request to merge your changes
